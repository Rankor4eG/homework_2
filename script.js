// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    },
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

console.log(books);

let root = document.querySelector("#root");
let ul_tag = document.createElement("ul");
let ul = root.appendChild(ul_tag);

function check__book({author, name, price}, index) {
  if (!author) {
      throw new Error(`Книга №${index + 1}. Автор невідомий`);
  }
  if (!name) {
      throw new Error(`Книга №${index + 1}. Назва невідома`);
  }
  if (!price) {
      throw new Error(`Книга №${index + 1}. Ціна невідома`);
  }
}

books.forEach((item, index) => {
    try {
        check__book(item, index);
        document.querySelector('ul').innerHTML += `<li>
          <p><strong>Книга №${index + 1}</strong></p> 
          <p>
            Автор: 
            <span> ${item.author}</span>
          </p>
          <p>
            Назва: 
            <span> ${item.name}</span>
          </p>
          <p>
            Ціна: 
            <span> ${item.price}$</span>
          </p>
        </li>`
    } catch (error) {
        console.log(error.message);
    }
});
